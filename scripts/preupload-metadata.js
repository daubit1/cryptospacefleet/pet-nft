const { uploadMetadataToIPFS } = require("./util/upload");
const { readFile, writeFile } = require("fs/promises");
const { getAttribsFromFilename } = require("./util/metadata");

const NUM_THREADS = 2;

let lookup = {};
let timestamp = Date.now();

const NFT_TYPE = "fleet";

if (process.argv.length < 3) {
  console.log(
    "missing parameter. example: node preupload-metadata.js <art-lookup-identifier> [metadata-lookup-identifier]"
  );
  process.exit(1);
}

function shuffle(array) {
  array.sort(() => Math.random() - 0.5);
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

const { getName } = require("./util/metadata");

async function preuploadMetadata() {
  const metadataTemplate = JSON.parse(
    await readFile("./data/metadatatemplate.json")
  );
  const imageIpfsLookup = JSON.parse(
    await readFile(`./data/cids/image-ipfs-lookup-${process.argv[2]}.json`)
  );
  const indivFilesExcludes = JSON.parse(
    await readFile("./data/indiv-exclude.json")
  );
  if (indivFilesExcludes.length >= 1 && indivFilesExcludes[0].endsWith("jpg")) {
    throw new Error("indivFilesExcludes does not end in jpg");
  }

  const lookuptableEntries = Object.entries(imageIpfsLookup).filter(
    (x) => !indivFilesExcludes.includes(x)
  );
  shuffle(lookuptableEntries);
  let i = 1;

  const metadataArray = [];
  for (const artlookup of lookuptableEntries) {
    const metadataAttribs = getAttribsFromFilename(NFT_TYPE, artlookup[0]);
    const metadata = {
      ...metadataTemplate,
      image: `ipfs://${artlookup[1]}`,
      attributes: metadataAttribs,
      name: getName(NFT_TYPE, metadataAttribs, metadataTemplate),
    };
    i++;
    metadataArray.push({ data: metadata, filename: artlookup[0] });
  }

  const batchLength = Math.floor(metadataArray.length / NUM_THREADS);
  const batch = [];
  let remaningToBatch = metadataArray;
  for (let i = 0; i < NUM_THREADS; i++) {
    let remaining = remaningToBatch.splice(batchLength);
    batch.push(remaningToBatch);
    remaningToBatch = remaining;
  }
  if (remaningToBatch.length > 0) {
    batch[batch.length - 1].push(...remaningToBatch);
  }

  const batchResult = await Promise.all(
    batch.map(async (metadatabatch) => {
      const batchLookup = {};
      for (const metadata of metadatabatch) {
        const ipfsMetaDataUploadResult = await uploadMetadataToIPFS(
          metadata.data
        );

        if (ipfsMetaDataUploadResult.IpfsHash) {
          batchLookup[metadata.filename] = ipfsMetaDataUploadResult.IpfsHash;
          console.log(
            `Uploaded metadata from ${metadata.filename} ${JSON.stringify(
              metadata
            )} to ipfs hash: ${ipfsMetaDataUploadResult.IpfsHash}`
          );
          await sleep(500);
        } else {
          throw new Error(
            `there was an error uploading metadata for ${
              metadata.filename
            } ${JSON.stringify(metadata)} to ipfs.`
          );
        }
      }
      return batchLookup;
    })
  );
  lookup = Object.assign(...batchResult);
}

if (process.argv.length >= 4) {
  timestamp = process.argv[3];
}

preuploadMetadata()
  .then(() => {
    writeFile(
      `./data/cids/metadata-ipfs-lookup-${timestamp}.json`,
      JSON.stringify(lookup)
    );
    console.log(
      `wrote cids to ./data/cids/metadata-ipfs-lookup-${timestamp}.json ${
        Object.keys(lookup).length
      }`
    );
  })
  .catch((err) => {
    console.log(err);
    writeFile(
      `./data/cids/metadata-ipfs-lookup-${timestamp}.json`,
      JSON.stringify(lookup)
    );
    console.log(
      `wrote cids to ./data/cids/metadata-ipfs-lookup-${timestamp}.json`
    );
    process.exit(1);
  });
