import { create } from "ipfs-http-client";
import { readFile } from "fs/promises";
import { uniq } from "lodash-es";

const nodeIndex = process.argv.findIndex((x) => x.includes("node"));
if (process.argv.length < nodeIndex + 3) {
  console.log(
    "missing parameter. example: node ipfs-pin-all.js <metadata-lookup-identifier> <image-lookup-identifier>"
  );
  process.exit(1);
}

async function pin() {
  const metadata = JSON.parse(
    await readFile(
      `./data/cids/metadata-ipfs-lookup-${process.argv[nodeIndex + 2]}.json`
    )
  );
  const arts = JSON.parse(
    await readFile(
      `./data/cids/image-ipfs-lookup-${process.argv[nodeIndex + 3]}.json`
    )
  );
  const cids = uniq([...Object.values(metadata), ...Object.values(arts)]);
  const client = create();
  for (const cid of cids) {
    const pinnedCid = await client.pin.add(cid);
    console.log(`Pinned CID: ${pinnedCid}`);
  }
}

pin()
  .then(() => {
    process.exit(1);
  })
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });
