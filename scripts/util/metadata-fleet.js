function getEyeColor(eyecolor) {
  switch (eyecolor) {
    case "EA":
      return "Brown";
    case "EB":
      return "Blue";
    case "EC":
      return "Green";
    default:
      throw new Error(`invalid eyecolor type ${eyecolor}`);
  }
}

function getSkinColor(race) {
  switch (race) {
    case "EU":
    case "E0":
      return "RGB(215,163,141)";
    case "AF":
    case "A0":
      return "RGB(63,53,45)";
    case "IT":
    case "I0":
      return "RGB(115,88,77)";
    default:
      throw new Error(`invalid race type ${race}`);
  }
}

function getWeapon(weapon) {
  switch (weapon) {
    case "G1":
      return "None"; // nix?
    case "G2":
      return "Pistol"; // pistol
    case "G3":
      return "Rifle"; // assault gun
    default:
      throw new Error(`invalid weapon type ${weapon}`);
  }
}

function getHairType(hairtype) {
  switch (hairtype) {
    case "MA":
      return "Short";
    case "MB":
      return "Surfer";
    case "MC":
      return "Fasson";
    case "MD":
      return "Bald"; // bald
    case "FA":
      return "Bob";
    case "FB":
      return "Long";

    default:
      throw new Error(`invalid hair type ${hairtype}`);
  }
}

function getHairColor(haircolor) {
  switch (haircolor) {
    case "CA":
      return "Blonde";
    case "CB":
      return "Grey";
    case "CC":
      return "Brown";

    default:
      throw new Error(`unknown hair color ${haircolor}`);
  }
}

function getFacialhairType(facialhair) {
  switch (facialhair) {
    case "LA":
      return "Shaved";
    case "LB":
      return "Circle Beard";
    case "LC":
      return "Full Beard";
    case "LD":
      return "Moustache";

    default:
      throw new Error(`unknown facial hair ${facialhair}`);
  }
}

function getArmourType(armour) {
  switch (armour) {
    case "D1":
      return "Armour 1";
    case "D2":
      return "Armour 2";

    default:
      throw new Error(`unknown armour ${armour}`);
  }
}

function getGlasses(facewear) {
  switch (facewear) {
    case "GA":
      return "Glasses 1";
    case "GB":
      return "Round Glasses";
    case "GC":
      return "Glasses";
    case "G0":
      return "None";
    default:
      throw new Error(`invalid glasses type ${facewear}`);
  }
}

function getPassengerGlasses(facewear) {
  switch (facewear) {
    case "GA":
      return "Sunglasses";
    case "GB":
      return "Glasses 3";
    default:
      throw new Error(`invalid glasses type ${facewear}`);
  }
}

function getMaleMedicGlasses(facewear) {
  switch (facewear) {
    case "G1":
      return "Goggles";
    case "G0":
      return "None";
    default:
      throw new Error(`invalid glasses type ${facewear}`);
  }
}

function getFemaleMedicGlasses(facewear) {
  switch (facewear) {
    case "GA":
      return "Gas Mask";
    default:
      return getGlasses(facewear);
  }
}

function getMaleEngineerGlasses(facewear) {
  switch (facewear) {
    case "G1":
      return "Glasses 1"; // crew
    case "G2":
      return "AR Glasses"; // officer
    case "G0":
      return "None";
    default:
      throw new Error(`invalid glasses type ${facewear}`);
  }
}

function getHelmet(helmet) {
  switch (helmet) {
    case "H0":
      return "Helmet 1";
    case "H1":
      return "Helmet 2";
    case "H2":
      return "Helmet 3";
    case "H3":
      return "Helmet 4";
    case "H4":
      return "Helmet 5";
    case "H5":
      return "Helmet 6";
    case "H6":
      return "Helmet 7";
    default:
      throw new Error(`invalid helmet type ${helmet}`);
  }
}

function getPassengerClothingColor(color) {
  switch (color) {
    case "S0":
      return "Olive Green";
    case "S1":
      return "Turquoise";
    default:
      throw new Error(`invalid color type ${color}`);
  }
}

function getPassengeJumpsuitColor(color) {
  switch (color) {
    case "U1":
      return "White";
    case "U2":
      return "Black";
    default:
      throw new Error(`invalid passengerundercolor ${color}`);
  }
}

function getPassengerFirstClassHoodieColor(color) {
  switch (color) {
    case "S0":
      return "None";
    case "S1":
      return "Turquoise";
    case "S3":
      return "Pink";
    default:
      throw new Error(`invalid color type ${color}`);
  }
}

const getMetadataFuncs = [
  getMetadataForActor0,
  getMetadataForActor1,
  getMetadataForActor2,
  getMetadataForActor3,
  getMetadataForActor4,
  getMetadataForActor5,
  getMetadataForActor6,
  getMetadataForActor7,
  getMetadataForActor8,
  getMetadataForActor9,
  getMetadataForActor10,
  getMetadataForActor11,
  getMetadataForActor12,
  getMetadataForActor13,
  getMetadataForActor14,
  getMetadataForActor15,
];

module.exports = {
  getFleetPersonAttribs: function (filename) {
    const filenameWoExt = filename.split(".")[0];
    const splitFilenamesM = filenameWoExt.split("-");
    const splitFilenamesU = splitFilenamesM[0].split("_");
    const actor = parseInt(splitFilenamesU[3]);

    return [
      { trait_type: "Category", value: "Fleet" },
      ...getMetadataFuncs[actor](splitFilenamesM, splitFilenamesU),
    ];
  },
};

function getMetadataForActor0(splitFilenamesM, splitFilenamesU) {
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Light Turquoise";
      break;
    case "B1":
      bgValue = "Orange";
      break;
    case "B2":
      bgValue = "Dark Blue";
      break;
    case "B3":
      bgValue = "Yellow";
      break;
    case "B4":
      bgValue = "Grey";
      break;
    case "B5":
      bgValue = "Bright Blue";
      break;
    case "B6":
      bgValue = "Beige";
      break;
    case "B7":
      bgValue = "Pink";
      break;
    case "B8":
      bgValue = "Bright Violet";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Female" },
    { trait_type: "Rank", value: "Crew" },
    { trait_type: "Role", value: "Stewardess" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[1]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Face Wear",
      value: getGlasses(splitFilenamesM[3]),
    },
    {
      trait_type: "Hair Style",
      value: getHairType(splitFilenamesM[4]),
    },
    {
      trait_type: "Hair Color",
      value: getHairColor(splitFilenamesM[5]),
    },
  ];
}

function getMetadataForActor1(splitFilenamesM, splitFilenamesU) {
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Grey";
      break;
    case "B1":
      bgValue = "Pink";
      break;
    case "B2":
      bgValue = "Orange";
      break;
    case "B3":
      bgValue = "Green";
      break;
    case "B4":
      bgValue = "Violet";
      break;
    case "B5":
      bgValue = "Dark Blue";
      break;
    case "B6":
      bgValue = "Yellow";
      break;
    case "B7":
      bgValue = "Turquoise";
      break;
    case "B8":
      bgValue = "Bright Blue";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Male" },
    { trait_type: "Rank", value: "Crew" },
    { trait_type: "Role", value: "Medic" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[1]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Face Wear",
      value: getMaleMedicGlasses(splitFilenamesM[3]),
    },
    {
      trait_type: "Hair Style",
      value: getHairType(splitFilenamesM[4]),
    },
    {
      trait_type: "Facial Hair Style",
      value: getFacialhairType(splitFilenamesM[5]),
    },
    {
      trait_type: "Hair Color",
      value: getHairColor(splitFilenamesM[6]),
    },
  ];
}

function getMetadataForActor2(splitFilenamesM, splitFilenamesU) {
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Turquoise";
      break;
    case "B1":
      bgValue = "Orange";
      break;
    case "B2":
      bgValue = "Dark Blue";
      break;
    case "B3":
      bgValue = "Yellow";
      break;
    case "B4":
      bgValue = "Grey";
      break;
    case "B5":
      bgValue = "Bright Blue";
      break;
    case "B6":
      bgValue = "Beige";
      break;
    case "B7":
      bgValue = "Pink";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Female" },
    { trait_type: "Rank", value: "Crew" },
    { trait_type: "Role", value: "Medic" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[1]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Face Wear",
      value: getFemaleMedicGlasses(splitFilenamesM[3]),
    },
    {
      trait_type: "Hair Style",
      value: getHairType(splitFilenamesM[4]),
    },
    {
      trait_type: "Hair Color",
      value: getHairColor(splitFilenamesM[5]),
    },
  ];
}

function getMetadataForActor3(splitFilenamesM, splitFilenamesU) {
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Grey";
      break;
    case "B1":
      bgValue = "Dark Blue";
      break;
    case "B2":
      bgValue = "Orange";
      break;
    case "B3":
      bgValue = "Pink";
      break;
    case "B4":
      bgValue = "Green";
      break;
    case "B5":
      bgValue = "Violet";
      break;
    case "B6":
      bgValue = "Yellow";
      break;
    case "B7":
      bgValue = "Turqoise";
      break;
    case "B8":
      bgValue = "Birght Blue";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Male" },
    { trait_type: "Rank", value: "Crew" },
    { trait_type: "Role", value: "Engineer" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[1]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Face Wear",
      value: getMaleEngineerGlasses(splitFilenamesM[3]),
    },
    {
      trait_type: "Hair Style",
      value: getHairType(splitFilenamesM[4]),
    },
    {
      trait_type: "Facial Hair Style",
      value: getFacialhairType(splitFilenamesM[5]),
    },
    {
      trait_type: "Hair Color",
      value: getHairColor(splitFilenamesM[6]),
    },
  ];
}

function getMetadataForActor4(splitFilenamesM, splitFilenamesU) {
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Grey";
      break;
    case "B1":
      bgValue = "Dark Blue";
      break;
    case "B2":
      bgValue = "Orange";
      break;
    case "B3":
      bgValue = "Pink";
      break;
    case "B4":
      bgValue = "Green";
      break;
    case "B5":
      bgValue = "Violet";
      break;
    case "B6":
      bgValue = "Yellow";
      break;
    case "B7":
      bgValue = "Bright Violet";
      break;
    case "B8":
      bgValue = "Bright Blue";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Female" },
    { trait_type: "Rank", value: "Crew" },
    { trait_type: "Role", value: "Engineer" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[1]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Face Wear",
      value: getGlasses(splitFilenamesM[3]),
    },
    {
      trait_type: "Hair Style",
      value: getHairType(splitFilenamesM[4]),
    },
    {
      trait_type: "Hair Color",
      value: getHairColor(splitFilenamesM[5]),
    },
  ];
}

function getMetadataForActor5(splitFilenamesM, splitFilenamesU) {
  const helmetKeys = [0, 1, 2, 3, 4, 5, 6].map((x) => `H${x}`);
  const hasHelmet = helmetKeys.includes(splitFilenamesM[4]);
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Grey";
      break;
    case "B1":
      bgValue = "Bright Turquoise";
      break;
    case "B2":
      bgValue = "Orange";
      break;
    case "B3":
      bgValue = "Yellow";
      break;
    case "B4":
      bgValue = "Dark Blue";
      break;
    case "B5":
      bgValue = "Violet";
      break;
    case "B6":
      bgValue = "Green";
      break;
    case "B7":
      bgValue = "Turquoise";
      break;
    case "B8":
      bgValue = "Olivine";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Male" },
    { trait_type: "Rank", value: "Crew" },
    { trait_type: "Role", value: "Soldier" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Armour",
      value: getArmourType(splitFilenamesM[1]),
    },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[3]),
    },

    ...(hasHelmet
      ? [
          {
            trait_type: "Face Wear",
            value: getHelmet(splitFilenamesM[4]),
          },
        ]
      : [
          {
            trait_type: "Hair Style",
            value: getHairType(splitFilenamesM[4]),
          },
          {
            trait_type: "Facial Hair Style",
            value: getFacialhairType(splitFilenamesM[5]),
          },
          {
            trait_type: "Hair Color",
            value: getHairColor(splitFilenamesM[6]),
          },
        ]),
    {
      trait_type: "Weapon",
      value: hasHelmet
        ? getWeapon(splitFilenamesM[5])
        : getWeapon(splitFilenamesM[7]),
    },
  ];
}

function getMetadataForActor6(splitFilenamesM, splitFilenamesU) {
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Grey";
      break;
    case "B1":
      bgValue = "Pink";
      break;
    case "B2":
      bgValue = "Yellow";
      break;
    case "B3":
      bgValue = "Green";
      break;
    case "B4":
      bgValue = "Violet";
      break;
    case "B5":
      bgValue = "Dark Blue";
      break;
    case "B6":
      bgValue = "Olivine";
      break;
    case "B7":
      bgValue = "Turquoise";
      break;
    case "B8":
      bgValue = "Bright Blue";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Male" },
    { trait_type: "Rank", value: "Officer" },
    { trait_type: "Role", value: "Medic" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[1]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Face Wear",
      value: getMaleMedicGlasses(splitFilenamesM[3]),
    },
    {
      trait_type: "Hair Style",
      value: getHairType(splitFilenamesM[4]),
    },
    {
      trait_type: "Facial Hair Style",
      value: getFacialhairType(splitFilenamesM[5]),
    },
    {
      trait_type: "Hair Color",
      value: getHairColor(splitFilenamesM[6]),
    },
  ];
}

function getMetadataForActor7(splitFilenamesM, splitFilenamesU) {
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Turquoise";
      break;
    case "B1":
      bgValue = "Yellow";
      break;
    case "B2":
      bgValue = "Dark Blue";
      break;
    case "B3":
      bgValue = "Green";
      break;
    case "B4":
      bgValue = "Grey";
      break;
    case "B5":
      bgValue = "Bright Blue";
      break;
    case "B6":
      bgValue = "Beige";
      break;
    case "B7":
      bgValue = "Pink";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Female" },
    { trait_type: "Rank", value: "Officer" },
    { trait_type: "Role", value: "Medic" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[1]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Face Wear",
      value: getFemaleMedicGlasses(splitFilenamesM[3]),
    },
    {
      trait_type: "Hair Style",
      value: getHairType(splitFilenamesM[4]),
    },
    {
      trait_type: "Hair Color",
      value: getHairColor(splitFilenamesM[5]),
    },
  ];
}

function getMetadataForActor8(splitFilenamesM, splitFilenamesU) {
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Grey";
      break;
    case "B1":
      bgValue = "Dark Blue";
      break;
    case "B2":
      bgValue = "Orange";
      break;
    case "B3":
      bgValue = "Pink";
      break;
    case "B4":
      bgValue = "Green";
      break;
    case "B5":
      bgValue = "Violet";
      break;
    case "B6":
      bgValue = "Yellow";
      break;
    case "B7":
      bgValue = "Turquoise";
      break;
    case "B8":
      bgValue = "Bright Blue";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Male" },
    { trait_type: "Rank", value: "Officer" },
    { trait_type: "Role", value: "Engineer" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[1]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Face Wear",
      value: getMaleEngineerGlasses(splitFilenamesM[3]),
    },
    {
      trait_type: "Hair Style",
      value: getHairType(splitFilenamesM[4]),
    },
    {
      trait_type: "Facial Hair Style",
      value: getFacialhairType(splitFilenamesM[5]),
    },
    {
      trait_type: "Hair Color",
      value: getHairColor(splitFilenamesM[6]),
    },
  ];
}

function getMetadataForActor9(splitFilenamesM, splitFilenamesU) {
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Grey";
      break;
    case "B1":
      bgValue = "Bright Blue";
      break;
    case "B2":
      bgValue = "Orange";
      break;
    case "B3":
      bgValue = "Pink";
      break;
    case "B4":
      bgValue = "Green";
      break;
    case "B5":
      bgValue = "Bright Violet";
      break;
    case "B6":
      bgValue = "Yellow";
      break;
    case "B7":
      bgValue = "Violet";
      break;
    case "B8":
      bgValue = "Turquoise";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Female" },
    { trait_type: "Rank", value: "Officer" },
    { trait_type: "Role", value: "Engineer" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[1]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Face Wear",
      value: getGlasses(splitFilenamesM[3]),
    },
    {
      trait_type: "Hair Style",
      value: getHairType(splitFilenamesM[4]),
    },
    {
      trait_type: "Hair Color",
      value: getHairColor(splitFilenamesM[5]),
    },
  ];
}

function getMetadataForActor10(splitFilenamesM, splitFilenamesU) {
  const helmetKeys = [0, 1, 2, 3].map((x) => `H${x}`);
  const hasHelmet = helmetKeys.includes(splitFilenamesM[4]);
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Grey";
      break;
    case "B1":
      bgValue = "Bright Turquoise";
      break;
    case "B2":
      bgValue = "Orange";
      break;
    case "B3":
      bgValue = "Yellow";
      break;
    case "B4":
      bgValue = "Bright Blue";
      break;
    case "B5":
      bgValue = "Violet";
      break;
    case "B6":
      bgValue = "Green";
      break;
    case "B7":
      bgValue = "Turquoise";
      break;
    case "B8":
      bgValue = "Olivine";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Male" },
    { trait_type: "Rank", value: "Officer" },
    { trait_type: "Role", value: "Soldier" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Armour",
      value: getArmourType(splitFilenamesM[1]),
    },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[3]),
    },
    ...(hasHelmet
      ? [
          {
            trait_type: "Face Wear",
            value: getHelmet(splitFilenamesM[4]),
          },
        ]
      : [
          {
            trait_type: "Hair Style",
            value: getHairType(splitFilenamesM[4]),
          },
          {
            trait_type: "Facial Hair Style",
            value: getFacialhairType(splitFilenamesM[5]),
          },
          {
            trait_type: "Hair Color",
            value: getHairColor(splitFilenamesM[6]),
          },
        ]),
    {
      trait_type: "Weapon",
      value: hasHelmet
        ? getWeapon(splitFilenamesM[5])
        : getWeapon(splitFilenamesM[7]),
    },
  ];
}

function getMetadataForActor11(splitFilenamesM, splitFilenamesU) {
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Grey";
      break;
    case "B1":
      bgValue = "Beige";
      break;
    case "B2":
      bgValue = "Bright Violet";
      break;
    case "B3":
      bgValue = "Orange";
      break;
    case "B4":
      bgValue = "Bright Blue";
      break;
    case "B5":
      bgValue = "Pink";
      break;
    case "B6":
      bgValue = "Green";
      break;
    case "B7":
      bgValue = "Violet";
      break;
    case "B8":
      bgValue = "Yellow";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Female" },
    { trait_type: "Rank", value: "Passenger" },
    { trait_type: "Role", value: "Economy" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[1]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Face Wear",
      value: getPassengerGlasses(splitFilenamesM[3]),
    },
    {
      trait_type: "Hair Style",
      value: getHairType(splitFilenamesM[4]),
    },
    {
      trait_type: "Hair Color",
      value: getHairColor(splitFilenamesM[5]),
    },
    {
      trait_type: "Clothing Color",
      value: getPassengerClothingColor(splitFilenamesM[6]),
    },
  ];
}

function getMetadataForActor12(splitFilenamesM, splitFilenamesU) {
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Grey";
      break;
    case "B1":
      bgValue = "Orange";
      break;
    case "B2":
      bgValue = "Dark Blue";
      break;
    case "B3":
      bgValue = "Bright Turquoise";
      break;
    case "B4":
      bgValue = "Turquoise";
      break;
    case "B5":
      bgValue = "Violet";
      break;
    case "B6":
      bgValue = "Green";
      break;
    case "B7":
      bgValue = "Bright Blue";
      break;
    case "B8":
      bgValue = "Yellow";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Male" },
    { trait_type: "Rank", value: "Passenger" },
    { trait_type: "Role", value: "Economy" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[1]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Hair Style",
      value: getHairType(splitFilenamesM[3]),
    },
    {
      trait_type: "Facial Hair Style",
      value: getFacialhairType(splitFilenamesM[4]),
    },
    {
      trait_type: "Hair Color",
      value: getHairColor(splitFilenamesM[5]),
    },
  ];
}

function getMetadataForActor13(splitFilenamesM, splitFilenamesU) {
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Grey";
      break;
    case "B1":
      bgValue = "Beige";
      break;
    case "B2":
      bgValue = "Bright Violet";
      break;
    case "B3":
      bgValue = "Orange";
      break;
    case "B4":
      bgValue = "Bright Blue";
      break;
    case "B5":
      bgValue = "Pink";
      break;
    case "B6":
      bgValue = "Green";
      break;
    case "B7":
      bgValue = "Violet";
      break;
    case "B8":
      bgValue = "Yellow";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Female" },
    { trait_type: "Rank", value: "Passenger" },
    { trait_type: "Role", value: "First Class" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[1]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Face Wear",
      value: getPassengerGlasses(splitFilenamesM[3]),
    },
    {
      trait_type: "Hair Style",
      value: getHairType(splitFilenamesM[4]),
    },
    {
      trait_type: "Hair Color",
      value: getHairColor(splitFilenamesM[5]),
    },
    {
      trait_type: "Jumpsuit Color",
      value: getPassengeJumpsuitColor(splitFilenamesM[6]),
    },
    {
      trait_type: "Hoodie Color",
      value: getPassengerFirstClassHoodieColor(splitFilenamesM[7]),
    },
  ];
}

function getMetadataForActor14(splitFilenamesM, splitFilenamesU) {
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Grey";
      break;
    case "B1":
      bgValue = "Orange";
      break;
    case "B2":
      bgValue = "Dark Blue";
      break;
    case "B3":
      bgValue = "Bright Turquoise";
      break;
    case "B4":
      bgValue = "Turquoise";
      break;
    case "B5":
      bgValue = "Violet";
      break;
    case "B6":
      bgValue = "Green";
      break;
    case "B7":
      bgValue = "Bright Blue";
      break;
    case "B8":
      bgValue = "Yellow";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Male" },
    { trait_type: "Rank", value: "Passenger" },
    { trait_type: "Role", value: "First Class" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[1]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[2]),
    },
    {
      trait_type: "Hair Style",
      value: getHairType(splitFilenamesM[3]),
    },
    {
      trait_type: "Facial Hair Style",
      value: getFacialhairType(splitFilenamesM[4]),
    },
    {
      trait_type: "Hair Color",
      value: getHairColor(splitFilenamesM[5]),
    },
  ];
}

function getMetadataForActor15(splitFilenamesM, splitFilenamesU) {
  const helmetKeys = [0, 1, 2].map((x) => `H${x}`);
  const hasHelmet = helmetKeys.includes(splitFilenamesM[5]);
  let bgValue;
  switch (splitFilenamesU[splitFilenamesU.length - 1]) {
    case "B0":
      bgValue = "Black";
      break;
    case "B1":
      bgValue = "Turquoise";
      break;
    case "B2":
      bgValue = "Green";
      break;
    case "B3":
      bgValue = "Red";
      break;
    case "B4":
      bgValue = "Bright Blue";
      break;
    case "B5":
      bgValue = "Pink";
      break;
    case "B6":
      bgValue = "Yellow";
      break;
    case "B7":
      bgValue = "Orange";
      break;
    case "B8":
      bgValue = "Dark Blue";
      break;
    default:
      throw new Error(
        `unknown background color ${
          splitFilenamesU[splitFilenamesU.length - 1]
        }`
      );
  }
  return [
    { trait_type: "Sex", value: "Male" },
    { trait_type: "Rank", value: "Captain" },
    { trait_type: "Role", value: "Captain" },
    { trait_type: "Background Color", value: bgValue },
    {
      trait_type: "Eye Color",
      value: getEyeColor(splitFilenamesM[1]),
    },
    {
      trait_type: "Skin Color",
      value: getSkinColor(splitFilenamesM[2]),
    },
    ...(hasHelmet
      ? [
          {
            trait_type: "Face Wear",
            value: getHelmet(splitFilenamesM[5]),
          },
        ]
      : [
          {
            trait_type: "Hair Style",
            value: getHairType(splitFilenamesM[5]),
          },
          {
            trait_type: "Facial Hair Style",
            value: getFacialhairType(splitFilenamesM[6]),
          },
          {
            trait_type: "Hair Color",
            value: getHairColor(splitFilenamesM[7]),
          },
        ]),
  ];
}
