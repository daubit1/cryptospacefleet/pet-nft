const pinataSDK = require("@pinata/sdk");

const pinataKey = process.env.PINATA_KEY;
const pinataKeySecret = process.env.PINATA_SECRECT_KEY;
const pinata = pinataSDK(pinataKey, pinataKeySecret);

async function unpinall() {
  const pinlist = await pinata.pinList();
  for (const pin of pinlist.rows) {
    //await pinata.unpin(pin.ipfs_pin_hash);
  }
}

unpinall()
  .then(() => process.exit(0))
  .catch((e) => {
    console.log(e);
    process.exit(1);
  });
