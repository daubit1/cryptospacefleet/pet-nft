function getRawColor(color) {
  switch (color) {
    case "BB":
      return "Black";
    case "WW":
      return "White";
    case "YE":
      return "Yellow";
    case "BL":
      return "Blue";
    case "RE":
      return "Red";
    case "GR":
      return "Green";
    case "VI":
      return "Violett";
    case "OR":
      return "Orange";
    case "BR":
      return "Brown";
    case "GY":
      return "Grey";
    case "BU":
      return "Bright Blue";
    default:
      throw new Error(`invalid color ${color}`);
  }
}

function getColor1(color) {
  return { trait_type: "Background Color", value: getRawColor(color) };
}

function getColor2(color) {
  return { trait_type: "Foreground Color", value: getRawColor(color) };
}

function getFur(fur) {
  let value = "";
  switch (fur) {
    case "FIC":
      value = "Icecream";
      break;
    case "FTI":
      value = "Tiger";
      break;
    case "FLE":
      value = "Leopard";
      break;
    case "FPA":
      value = "Panda";
      break;
    case "FSK":
      value = "Skull";
      break;
    default:
      throw new Error(`invalid fur type ${fur}`);
  }
  return { trait_type: "Fur", value: value };
}

function getPodest(podest) {
  let value = "";
  switch (podest) {
    case "PGB":
      value = "Grey Bright";
      break;
    case "PGM":
      value = "Grey Middle";
      break;
    case "PGD":
      value = "Grey Dark";
      break;
    case "PGV":
      value = "Grey very Dark";
      break;
    default:
      throw new Error(`invalid podest type ${podest}`);
  }
  return { trait_type: "Podest", value: value };
}

function getEyeColor(eyecolor) {
  let value = "";
  switch (eyecolor) {
    case "EYE":
      value = "Yellow";
      break;
    case "EBB":
      value = "Blue Bright";
      break;
    case "EBD":
      value = "Blue Dark";
      break;
    case "ERE":
      value = "Red";
      break;
    case "EGR":
      value = "Green";
      break;
    case "EOR":
      value = "Orange";
      break;
    case "EVI":
      value = "Violett";
      break;
    default:
      throw new Error(`invalid eyecolor type ${eyecolor}`);
  }
  return { trait_type: "Eyecolor", value: value };
}

module.exports = {
  getPetAttribs: function (filename) {
    const filenameWoExt = filename.split(".")[0];
    const splitFilenames = filenameWoExt.split("-");
    if (splitFilenames.length !== 6) {
      throw new Error(
        `filename ${filename} does not match pet scheme, length ${splitFilenames.length}`
      );
    }
    if (splitFilenames[0] !== "CSF") {
      throw new Error(`Expected CSF got ${splitFilenames[0]}`);
    }
    const attribs = [];
    attribs.push({ trait_type: "Category", value: "Kitty" });
    attribs.push(getFur(splitFilenames[1]));
    attribs.push(getColor1(splitFilenames[2]));
    attribs.push(getColor2(splitFilenames[3]));
    attribs.push(getPodest(splitFilenames[4]));
    attribs.push(getEyeColor(splitFilenames[5]));
    return attribs;
  },
};
