const { uploadMetadataToIPFS, uploadFileToIPFS } = require("./upload");

const result1 = uploadFileToIPFS("./art.png");

result1
  .then((result1) => {
    const result = uploadMetadataToIPFS({
      description:
        "Friendly OpenSea Creature that enjoys long swims in the ocean.",
      external_url: "https://openseacreatures.io/3",
      image: "ipfs://" + result1.IpfsHash,
      name: "Dave Starbelly",
      attributes: [],
    });

    result
      .then((result) => console.log(result))
      .catch((err) => console.log(err));
  })
  .catch((err) => console.log(err));
