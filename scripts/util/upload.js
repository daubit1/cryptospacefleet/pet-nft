const fs = require("fs");
const pinataSDK = require("@pinata/sdk");

const pinataKey = process.env.PINATA_KEY;
const pinataKeySecret = process.env.PINATA_SECRECT_KEY;
const pinata = pinataSDK(pinataKey, pinataKeySecret);

module.exports = {
  uploadMetadataToIPFS: async function (data) {
    return pinata.pinJSONToIPFS(data);
  },
  uploadFileToIPFS: async function (filepath) {
    const readableStreamForFile = fs.createReadStream(filepath);
    return pinata.pinFileToIPFS(readableStreamForFile);
  },
};
