function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

module.exports = {
  getRandomInt: getRandomInt,
  removeRandomFromArray: function (arr) {
    const index = getRandomInt(0, arr.length - 1);
    const elem = arr.splice(index, 1);
    return { elem: elem[0], arr: arr };
  },
};
