const { getPetAttribs } = require("./metadata-pet");
const { getFleetPersonAttribs } = require("./metadata-fleet");

module.exports = {
  getAttribsFromFilename: function (type, filename) {
    if (type === "pet") {
      return getPetAttribs(filename);
    } else if (type === "fleet") {
      return getFleetPersonAttribs(filename);
    } else {
      console.log(`invalid type ${type}`);
      process.exit(1);
    }
  },
  getName: function (category, metadataAttribs, metadataTemplate) {
    if (category === "pet") {
      return `${metadataAttribs[2].value}-${metadataAttribs[3].value} ${metadataAttribs[1].value} ${metadataTemplate.name}`;
    } else if (category === "fleet") {
      const gender = metadataAttribs.find((x) => x.trait_type === "Sex").value;
      const rank = metadataAttribs.find((x) => x.trait_type === "Rank").value;
      const role = metadataAttribs.find((x) => x.trait_type === "Role").value;
      if (role === "Captain") {
        return `${role}`;
      } else if (rank === role) {
        return `${gender} ${rank}`;
      } else {
        return `${gender} ${rank} ${role}`;
      }
    } else {
      throw new Error(`unknown category ${category}`);
    }
  },
};
