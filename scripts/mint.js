require("dotenv").config();
const { readFile, readdir, writeFile } = require("fs/promises");
const { uploadFileToIPFS, uploadMetadataToIPFS } = require("./util/upload");
const { getAttribsFromFilename } = require("./util/metadata");
const { removeRandomFromArray } = require("./util/utils");

const HDWalletProvider = require("truffle-hdwallet-provider");
const web3 = require("web3");
const MNEMONIC = process.env.MNEMONIC;
const NODE_API_KEY = process.env.INFURA_KEY || process.env.ALCHEMY_KEY;
const PROVIDER = process.env.PROVIDER;
const NFT_CONTRACT_ADDRESS = process.env.NFT_CONTRACT_ADDRESS;
const OWNER_ADDRESS = process.env.OWNER_ADDRESS;
const NETWORK = process.env.NETWORK;

if (!MNEMONIC || !NODE_API_KEY || !OWNER_ADDRESS || !NETWORK) {
  console.error(
    "Please set a mnemonic, Alchemy/Infura key, owner, network, and contract address."
  );
  return;
}

const network =
  NETWORK === "mainnet" || NETWORK === "mumbai" || NETWORK === "live"
    ? "mainnet"
    : "rinkeby";
let provider;
switch (PROVIDER) {
  case "infura":
    provider = "https://" + network + ".infura.io/v3/" + NODE_API_KEY;
    break;
  case "alchemy":
    provider = "https://eth-" + network + ".alchemyapi.io/v2/" + NODE_API_KEY;
    break;
  case "alchemy-mumbai":
    provider =
      "https://polygon-" + network + ".g.alchemy.com/v2/" + NODE_API_KEY;
    break;
}

const wallet = new HDWalletProvider(MNEMONIC, provider);
const web3Instance = new web3(wallet);

// TODO: check ABI
const NFT_ABI = [
  {
    constant: false,
    inputs: [
      {
        name: "_to",
        type: "address",
      },
      {
        name: "_tokenURI",
        type: "string",
      },
    ],
    name: "mintTo",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
];

const nftContract = new web3Instance.eth.Contract(
  NFT_ABI,
  NFT_CONTRACT_ADDRESS,
  { gasLimit: "1000000" }
);

async function uploadArtToIPFS(art, imagePreload, imagePreloadLookuptable) {
  if (imagePreload) {
    const elem = imagePreloadLookuptable[art];
    if (!elem) {
      throw new Error(
        `--preloadedImages but ${art} not found in lookup-${imagePreload}.json`
      );
    }
    console.log(
      `Looked up ${art} result: ${JSON.stringify({ IpfsHash: elem })}`
    );
    return { IpfsHash: elem };
  } else {
    const ipfsArtUploadResult = await uploadFileToIPFS(`./data/art/${art}`);

    console.log(
      `Uploaded ${art} result: ${JSON.stringify(ipfsArtUploadResult)}`
    );
    return ipfsArtUploadResult;
  }
}

async function mintAll(imagePreload) {
  const targets = JSON.parse(await readFile("./data/addresses.json"));

  let allPictures = await readdir("./data/art");
  const index = allPictures.indexOf(".gitkeep");
  if (index > -1) {
    allPictures.splice(index, 1);
  }

  if (targets.length !== allPictures.length) {
    console.log(
      `targets and pictures length dont match targets: ${targets.length} pictures: ${pictures.length}`
    );
    return;
  }

  const metadataTemplate = JSON.parse(
    await readFile("./data/metadatatemplate.json")
  );

  const imagePreloadLookuptable = imagePreload
    ? JSON.parse(
        await readFile(`./data/cids/image-ipfs-lookup-${imagePreload}.json`)
      )
    : undefined;
  console.log(imagePreload);
  console.log(imagePreloadLookuptable);

  const cids = [];
  for (const address of targets) {
    console.log(`minting to ${address}`);

    const { elem, arr } = removeRandomFromArray(allPictures);
    allPictures = arr;
    const art = elem;
    const ipfsArtUploadResult = await uploadArtToIPFS(
      art,
      imagePreload,
      imagePreloadLookuptable
    );

    if (ipfsArtUploadResult.IpfsHash) {
      cids.push(ipfsArtUploadResult.IpfsHash);
      metadataTemplate.image = `ipfs://${ipfsArtUploadResult.IpfsHash}`;
      metadataTemplate.attributes = getAttribsFromFilename("pet", art);

      const ipfsMetaDataUploadResult = await uploadMetadataToIPFS(
        metadataTemplate
      );

      console.log(
        `Uploaded ${JSON.stringify(metadataTemplate)} result: ${JSON.stringify(
          ipfsMetaDataUploadResult
        )}`
      );

      if (ipfsMetaDataUploadResult.IpfsHash) {
        cids.push(ipfsMetaDataUploadResult.IpfsHash);
        const mintResult = await nftContract.methods
          .mintTo(address, ipfsMetaDataUploadResult.IpfsHash)
          .send({ from: OWNER_ADDRESS });

        console.log(mintResult);
        console.log(
          `minted token to ${address} tx: ${mintResult.transactionHash}`
        );
      } else {
        const timestamp = Date.now();
        writeFile(`./data/cids/cids-${timestamp}.json`, JSON.stringify(cids));
        console.log(`wrote cids to ./data/cids/cids-${timestamp}.json`);
        console.log(
          `there was an error uploading ${metadata} to ipfs. please remove art pin ${ipfsArtUploadResult.IpfsHash}`
        );
        return;
      }
    } else {
      const timestamp = Date.now();
      writeFile(`./data/cids/cids-${timestamp}.json`, JSON.stringify(cids));
      console.log(`wrote cids to ./data/cids/cids-${timestamp}.json`);
      console.log(`there was an error uploading ${art} to ipfs`);
      return;
    }
  }

  const timestamp = Date.now();
  writeFile(`./data/cids/cids-${timestamp}.json`, JSON.stringify(cids));
  console.log(`wrote cids to ./data/cids/cids-${timestamp}.json`);
}

const preloadedImagesFlagIndex = process.argv.indexOf("--preloadedImages");
let preloadedImagesFlag = undefined;
if (preloadedImagesFlagIndex > -1) {
  if (process.argv.length - 1 > preloadedImagesFlagIndex) {
    preloadedImagesFlag = process.argv[preloadedImagesFlagIndex + 1];
    // todo: check if process.argv[preloadedImagesFlagIndex + 1] is valid
  } else {
    console.log("--preloadedImages without timestamp, ignoring");
  }
}

mintAll(preloadedImagesFlag).catch((err) => console.log(err));
