const nodeIndex = process.argv.findIndex((x) => x.includes("node"));
if (process.argv.length < nodeIndex + 3) {
  console.log(
    "missing parameter. example: node mint-from-preupload.js <batch-identifier> <batch>"
  );
  process.exit(1);
}

require("dotenv").config();

const HDWalletProvider = require("@truffle/hdwallet-provider");
const web3 = require("web3");
const MNEMONIC = process.env.MNEMONIC;
const TESTNET_API_KEY = process.env.ALCHEMY_KEY_TESTNET;
const MAINNET_API_KEY = process.env.ALCHEMY_KEY_MAINNET;
const NFT_CONTRACT_ADDRESS = process.env.NFT_CONTRACT_ADDRESS;
const OWNER_ADDRESS = process.env.OWNER_ADDRESS;
const NETWORK = process.env.NETWORK;

if (!MNEMONIC && (!ALCHEMY_KEY_MAINNET || !ALCHEMY_KEY_TESTNET) && !NETWORK) {
  console.error(
    "Please set a mnemonic, Alchemy/Infura key, owner, network, and contract address."
  );
  return;
}

let provider;
switch (NETWORK) {
  case "mumbai":
    provider = "wss://polygon-mumbai.g.alchemy.com/v2/" + TESTNET_API_KEY;
    break;
  case "polygon":
    provider = "wss://polygon-mainnet.g.alchemy.com/v2/" + MAINNET_API_KEY;
    break;
}

const wallet = new HDWalletProvider(MNEMONIC, provider);
const web3Instance = new web3(wallet);

const NFT_ABI = [
  {
    inputs: [
      {
        internalType: "address",
        name: "_to",
        type: "address",
      },
      {
        internalType: "string",
        name: "_tokenURI",
        type: "string",
      },
    ],
    name: "mintTo",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
];

const { readFile } = require("fs/promises");
const { writeFileSync, unlinkSync } = require("fs");

const usedFilenames = [];

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function mint(batchIdentifier, batch) {
  const entries = JSON.parse(
    await readFile(`./data/addressBatch/${batchIdentifier}/${batch}.json`)
  );

  const gasPrice = await web3Instance.eth.getGasPrice();
  console.log(`gasprice: ${web3.utils.fromWei(gasPrice, "gwei")} gwei`);
  console.log(`gasprice: ${gasPrice} wei`);
  //const gasPrice2 = gasPrice + "000";
  //console.log(`gasprice: ${web3.utils.fromWei(gasPrice2, "gwei")} gwei`);
  console.log(`gasprice: ${web3.utils.toWei("100", "gwei")} wei`);
  //console.log(`gasprice: ${gasPrice2} wei`);
  const nftContract = new web3Instance.eth.Contract(
    NFT_ABI,
    NFT_CONTRACT_ADDRESS,
    { gasLimit: "1000000", gasPrice: web3.utils.toWei("50", "gwei") }
  );

  console.log(`contract: ${NFT_CONTRACT_ADDRESS}`);
  console.log(`network: ${NETWORK}`);
  console.log(`provider: ${provider}`);

  await sleep(4000);

  for (const data of entries) {
    try {
      console.log(
        `start minting token to ${data.address} with metadata ${data.metadata}`
      );
      console.log(data);

      const mintResult = await nftContract.methods
        .mintTo(data.address, data.metadata)
        .send({ from: OWNER_ADDRESS });

      console.log(
        `minted token to ${data.address} tx: ${mintResult.transactionHash}`
      );
      usedFilenames.push(data.filename);
    } catch (err) {
      writeFileSync("./usedFilenames.json", JSON.stringify(usedFilenames));
      throw err;
    }
  }

  //unlinkSync(`./data/addressBatch/${batchIdentifier}/${batch}.json`);
}

mint(process.argv[nodeIndex + 2], process.argv[nodeIndex + 3])
  .then(() => {
    writeFileSync("./usedFilenames.json", JSON.stringify(usedFilenames));
    process.exit(0);
  })
  .catch((err) => {
    console.log(err);
    writeFileSync("./usedFilenames.json", JSON.stringify(usedFilenames));
    process.exit(1);
  });
