const { readFile, writeFile, readdir } = require("fs/promises");
const { getAttribsFromFilename } = require("./util/metadata");

let lookup = {};

const NFT_TYPE = "fleet";

function shuffle(array) {
  array.sort(() => Math.random() - 0.5);
}

const { getName } = require("./util/metadata");

async function preuploadMetadata() {
  const metadataTemplate = JSON.parse(
    await readFile("./data/metadatatemplate.json")
  );
  const indivFilesExcludes = JSON.parse(
    await readFile("./data/indiv-exclude.json")
  );
  const artFiles = (await readdir("./data/art")).filter(
    (a) => !a.includes(".gitkeep")
  );
  const fakeLookup = {};
  artFiles.forEach((a) => (fakeLookup[a] = a));

  const lookuptableEntries = Object.entries(fakeLookup).filter(
    (x) => !indivFilesExcludes.includes(x)
  );
  shuffle(lookuptableEntries);
  let i = 1;

  const metadataArray = [];
  for (const artlookup of lookuptableEntries) {
    const metadataAttribs = getAttribsFromFilename(NFT_TYPE, artlookup[0]);
    const metadata = {
      ...metadataTemplate,
      image: `ipfs://${artlookup[1]}`,
      attributes: metadataAttribs,
      name: getName(NFT_TYPE, metadataAttribs, metadataTemplate),
    };
    i++;
    metadataArray.push({ data: metadata, filename: artlookup[0] });
  }

  lookup = metadataArray;
}

preuploadMetadata()
  .then(() => {
    writeFile(`./data/metadata-dryrun.json`, JSON.stringify(lookup));
  })
  .catch((err) => {
    console.log(err);
    writeFile(`./data/metadata-dryrun.json`, JSON.stringify(lookup));
    process.exit(1);
  });
