const { readdir, writeFile } = require("fs/promises");
const { uploadFileToIPFS } = require("./util/upload");

const NUM_THREADS = 2;

let lookup = {};
let timestamp = Date.now();

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function preuploadArt() {
  let allPictures = await readdir("./data/art");
  const index = allPictures.indexOf(".gitkeep");
  if (index > -1) {
    allPictures.splice(index, 1);
  }
  const batchLength = Math.floor(allPictures.length / NUM_THREADS);
  const batch = [];
  let remaningToBatch = allPictures;
  for (let i = 0; i < NUM_THREADS; i++) {
    let remaining = remaningToBatch.splice(batchLength);
    batch.push(remaningToBatch);
    remaningToBatch = remaining;
  }
  if (remaningToBatch.length > 0) {
    batch[batch.length - 1].push(...remaningToBatch);
  }

  const batchResult = await Promise.all(
    batch.map(async (arts) => {
      const batchLookup = {};
      for (const art of arts) {
        const ipfsArtUploadResult = await uploadFileToIPFS(`./data/art/${art}`);

        console.log(
          `Uploaded ${art} result: ${JSON.stringify(ipfsArtUploadResult)}`
        );

        if (ipfsArtUploadResult.IpfsHash) {
          batchLookup[art] = ipfsArtUploadResult.IpfsHash;
          console.log(
            `Uploaded ${art} to ipfs hash: ${ipfsArtUploadResult.IpfsHash}`
          );
          await sleep(500);
        } else {
          throw new Error(`there was an error uploading ${art} to ipfs.`);
        }
      }
      return batchLookup;
    })
  );
  lookup = Object.assign(...batchResult);
}

if (process.argv.length >= 3) {
  timestamp = process.argv[2];
}

preuploadArt()
  .then(() => {
    writeFile(
      `./data/cids/image-ipfs-lookup-${timestamp}.json`,
      JSON.stringify(lookup)
    );
    console.log(
      `wrote cids to ./data/cids/image-ipfs-lookup-${timestamp}.json ${
        Object.keys(lookup).length
      }`
    );
  })
  .catch((err) => {
    console.log(err);
    writeFile(
      `./data/cids/image-ipfs-lookup-${timestamp}.json`,
      JSON.stringify(lookup)
    );
    console.log(
      `wrote cids to ./data/cids/image-ipfs-lookup-${timestamp}.json`
    );
    process.exit(1);
  });
