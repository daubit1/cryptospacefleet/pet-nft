require("dotenv").config();

const HDWalletProvider = require("@truffle/hdwallet-provider");
const web3 = require("web3");
const MNEMONIC = process.env.MNEMONIC;
const NODE_API_KEY = process.env.INFURA_KEY || process.env.ALCHEMY_KEY;
const PROVIDER = process.env.PROVIDER;
const NFT_CONTRACT_ADDRESS = process.env.NFT_CONTRACT_ADDRESS;
const OWNER_ADDRESS = process.env.OWNER_ADDRESS;
const NETWORK = process.env.NETWORK;

const NFT_ABI = [
  {
    inputs: [
      {
        internalType: "address",
        name: "_logic",
        type: "address",
      },
      {
        internalType: "address",
        name: "admin_",
        type: "address",
      },
      {
        internalType: "bytes",
        name: "_data",
        type: "bytes",
      },
    ],
    stateMutability: "nonpayable",
    type: "constructor",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        internalType: "address",
        name: "previousAdmin",
        type: "address",
      },
      {
        indexed: false,
        internalType: "address",
        name: "newAdmin",
        type: "address",
      },
    ],
    name: "AdminChanged",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "beacon",
        type: "address",
      },
    ],
    name: "BeaconUpgraded",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "implementation",
        type: "address",
      },
    ],
    name: "Upgraded",
    type: "event",
  },
  {
    stateMutability: "payable",
    type: "fallback",
    payable: true,
  },
  {
    inputs: [],
    name: "admin",
    outputs: [
      {
        internalType: "address",
        name: "admin_",
        type: "address",
      },
    ],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "newAdmin",
        type: "address",
      },
    ],
    name: "changeAdmin",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [],
    name: "implementation",
    outputs: [
      {
        internalType: "address",
        name: "implementation_",
        type: "address",
      },
    ],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "newImplementation",
        type: "address",
      },
    ],
    name: "upgradeTo",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "newImplementation",
        type: "address",
      },
      {
        internalType: "bytes",
        name: "data",
        type: "bytes",
      },
    ],
    name: "upgradeToAndCall",
    outputs: [],
    stateMutability: "payable",
    type: "function",
    payable: true,
  },
  {
    stateMutability: "payable",
    type: "receive",
    payable: true,
  },
];

if (!MNEMONIC || !NODE_API_KEY || !OWNER_ADDRESS || !NETWORK) {
  console.error(
    "Please set a mnemonic, Alchemy/Infura key, owner, network, and contract address."
  );
  return;
}

let network;
switch (NETWORK) {
  case "mainnet":
  case "live":
    network = "mainnet";
    break;
  case "mumbai":
    network = NETWORK;
}
let provider;
switch (PROVIDER) {
  case "infura":
    provider = "https://" + network + ".infura.io/v3/" + NODE_API_KEY;
    break;
  case "alchemy":
    provider = "https://eth-" + network + ".alchemyapi.io/v2/" + NODE_API_KEY;
    break;
  case "alchemy-mumbai":
    provider =
      "https://polygon-" + network + ".g.alchemy.com/v2/" + NODE_API_KEY;
    break;
}

const wallet = new HDWalletProvider(MNEMONIC, provider);
const web3Instance = new web3(wallet);

const nftContract = new web3Instance.eth.Contract(
  NFT_ABI,
  "0x10acaa621Cc3955804D21e97d1B639305dbD7B23",
  { gasLimit: "1000000" }
);

nftContract.methods
  .changeAdmin("0x1aDce6bcc3f5127c760481E0EF270307DeD604A6")
  .send({ from: OWNER_ADDRESS })
  .then(() => console.log("done"))
  .catch((err) => console.log(err));
