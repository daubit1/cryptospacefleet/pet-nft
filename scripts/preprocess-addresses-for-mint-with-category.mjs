import { readFile, writeFile, mkdir } from "fs/promises";
import { existsSync, readFileSync } from "fs";

const PassengerActors = [11, 12, 13, 14];
const CrewActors = [1, 2, 3, 4, 5];
const OfficerActors = [6, 7, 8, 9, 10];
const CaptainActors = [15];

const PassengerId = 50;
const CrewId = 51;
const OfficerId = 52;
const CaptainId = 53;

function getActorNumber(filename) {
  return parseInt(filename.split("_")[3]);
}

async function preprocess(lookupsuffix, batchIdentifier, usedArt) {
  if (!existsSync("./data/addressBatch")) {
    await mkdir("./data/addressBatch");
  }

  if (!existsSync(`./data/addressBatch/${batchIdentifier}`)) {
    await mkdir(`./data/addressBatch/${batchIdentifier}`);
  }

  const addressesRaw = JSON.parse(
    await readFile("./data/addresses.processed.2.json")
  );
  const indiv = JSON.parse(await readFile("./data/indiv.json")).map((x) => {
    return {
      ...x,
      address: JSON.parse(x.custom_field)["1"],
    };
  });

  const addresses = addressesRaw
    // remove indiv option and their nfts
    .map((x) => {
      const indivForThisAddress = indiv.filter((y) => y.address === x.address);
      if (indivForThisAddress.length > 0) {
        const products = x.products;
        for (const option of indivForThisAddress) {
          const index = products.findIndex((z) => z === option.product_id);
          if (index > -1) {
            console.log(
              `splice option ${JSON.stringify(option)} for ${JSON.stringify(x)}`
            );
            products.splice(index, 1);
          } else {
            throw new Error(
              `trying to delete ${JSON.stringify(option)} on ${JSON.stringify(
                x
              )} but did not find product`
            );
          }
        }
        return {
          ...x,
          products: x.products,
        };
      }
      return x;
    })
    .map((x) => {
      return { ...x, products: x.products.filter((y) => y !== 54 && y !== 55) };
    })
    .filter((x) => x.products.length > 0);

  const lookupTable = JSON.parse(
    await readFile(`./data/cids/metadata-ipfs-lookup-${lookupsuffix}.json`)
  );
  const indivFilesExcludes = JSON.parse(
    await readFile("./data/indiv-exclude.json")
  );

  const allArts = Object.keys(lookupTable)
    .filter((x) => !indivFilesExcludes.includes(x))
    .filter((x) => !usedArt.includes(x));
  const passengerArts = allArts.filter((x) =>
    PassengerActors.includes(getActorNumber(x))
  );
  const crewArts = allArts.filter((x) =>
    CrewActors.includes(getActorNumber(x))
  );
  const officerArts = allArts.filter((x) =>
    OfficerActors.includes(getActorNumber(x))
  );
  const captainArts = allArts.filter((x) =>
    CaptainActors.includes(getActorNumber(x))
  );
  const nfts = addresses.reduce((prev, cur) => prev + cur.products.length, 0);
  const unique =
    nfts <=
    passengerArts.length +
      crewArts.length +
      officerArts.length +
      captainArts.length;
  if (!unique) {
    throw new Error(
      `not enough art, tried to print ${nfts} nfts, but only got ${arts.length} art`
    );
  }

  const addrToArt = [];
  let passengerIndex = 0;
  let crewIndex = 0;
  let officerIndex = 0;
  let captainIndex = 0;
  for (const address of addresses) {
    for (const product of address.products) {
      let elem = "";
      switch (product) {
        case PassengerId:
          elem = passengerArts[passengerIndex];
          passengerIndex++;
          break;
        case CrewId:
          elem = crewArts[crewIndex];
          crewIndex++;
          break;
        case OfficerId:
          elem = officerArts[officerIndex];
          officerIndex++;
          break;
        case CaptainId:
          elem = captainArts[captainIndex];
          captainIndex++;
          break;
        default:
          throw new Error(`unknown product category ${product}`);
      }
      const lookup = lookupTable[elem];
      if (!lookup) {
        throw new Error(`tried to lookup ${elem} but result was undefined`);
      }
      console.log(`${elem} ${lookup}`);
      addrToArt.push({
        raw: address,
        address: address.address,
        metadata: lookup,
        filename: elem,
        product: product,
      });
    }
  }

  await writeFile(
    `./data/addressBatch/${batchIdentifier}-all.json`,
    JSON.stringify(addrToArt)
  );
  await writeFile(
    `./data/addressBatch/${batchIdentifier}-usedart.json`,
    JSON.stringify([
      ...indivFilesExcludes,
      ...usedArt,
      ...addrToArt.map((x) => x.filename),
    ])
  );

  const batches = [];
  function take(arr, len) {
    return arr.splice(0, len);
  }
  while (addrToArt.length > 0) {
    batches.push(take(addrToArt, 20));
  }
  for (let i = 0; i < batches.length; i++) {
    const batch = batches[i];
    await writeFile(
      `./data/addressBatch/${batchIdentifier}/${i}.json`,
      JSON.stringify(batch)
    );
  }
}

const nodeIndex = process.argv.findIndex((x) => x.includes("node"));
if (process.argv.length < nodeIndex + 3) {
  console.log(
    "missing parameter. example: node preprocess-addresses-for-mint.mjs <metadata-lookup-identifier> <batch-identifier> [last-batch]"
  );
  process.exit(1);
}

let usedArt = [];
if (process.argv.length > nodeIndex + 4) {
  usedArt = JSON.parse(
    readFileSync(
      `./data/addressBatch/${process.argv[nodeIndex + 4]}-usedart.json`
    )
  );
}

preprocess(process.argv[nodeIndex + 2], process.argv[nodeIndex + 3], usedArt)
  .then(() => process.exit(0))
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });
