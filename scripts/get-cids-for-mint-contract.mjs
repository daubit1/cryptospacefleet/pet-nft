import { readFile, writeFile, mkdir } from "fs/promises";

function take(arr, len) {
  return arr.splice(0, len);
}

async function preprocess(lookupsuffix, batchIdentifier, nextBatchIdentifier) {
  const usedArt = JSON.parse(
    await readFile(`./data/addressBatch/${batchIdentifier}-usedart.json`)
  );
  const metadata = JSON.parse(
    await readFile(`./data/cids/metadata-ipfs-lookup-${lookupsuffix}.json`)
  );

  const entries = Object.entries(metadata);
  const entriesWoUsedArt = entries.filter(
    ([key, value]) => !usedArt.includes(key)
  );
  const newMetadata = take(entriesWoUsedArt, entries.length - 3000);
  await writeFile(
    "./data/mint-contract-cids.json",
    JSON.stringify(newMetadata)
  );
  const newUsedArt = [...usedArt, ...newMetadata.map(([key, value]) => key)];
  await writeFile(
    `./data/addressBatch/${nextBatchIdentifier}-usedart.json`,
    JSON.stringify(newUsedArt)
  );
}

const nodeIndex = process.argv.findIndex((x) => x.includes("node"));
if (process.argv.length < nodeIndex + 4) {
  console.log(
    "missing parameter. example: node get-cids-for-mint-contract.mjs <metadata-lookup-identifier> <batch-identifier> <next-batch-identifier>"
  );
  process.exit(1);
}

preprocess(
  process.argv[nodeIndex + 2],
  process.argv[nodeIndex + 3],
  process.argv[nodeIndex + 4]
)
  .then(() => process.exit(0))
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });
