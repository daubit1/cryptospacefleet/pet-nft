import { readFile, writeFile, mkdir } from "fs/promises";
import { existsSync, readFileSync } from "fs";

async function preprocess(lookupsuffix, batchIdentifier, usedArt) {
  if (!existsSync("./data/addressBatch")) {
    await mkdir("./data/addressBatch");
  }

  if (!existsSync(`./data/addressBatch/${batchIdentifier}`)) {
    await mkdir(`./data/addressBatch/${batchIdentifier}`);
  }

  const addressesRaw = JSON.parse(
    await readFile("./data/addresses.processed.2.json")
  );

  const addresses = addressesRaw
    .map((x) => {
      return { ...x, products: x.products.filter((y) => y !== 54 && y !== 55) };
    })
    .filter((x) => x.products.length > 0);
  const lookupTable = JSON.parse(
    await readFile(`./data/cids/metadata-ipfs-lookup-${lookupsuffix}.json`)
  );
  const indivFilesExcludes = JSON.parse(
    await readFile("./data/indiv-exclude.json")
  );

  const arts = Object.keys(lookupTable)
    .filter((x) => !indivFilesExcludes.includes(x))
    .filter((x) => !usedArt.includes(x));
  const nfts = addresses.reduce((prev, cur) => prev + cur.products.length, 0);
  const unique = nfts <= arts.length;
  if (!unique) {
    throw new Error(
      `not enough art, tried to print ${nfts} nfts, but only got ${arts.length} art`
    );
  }

  const addrToArt = [];
  let i = 0;
  console.log(addresses);
  for (const address of addresses) {
    for (const product of address.products) {
      const elem = arts[i];
      i++;
      const lookup = lookupTable[elem];
      if (!lookup) {
        throw new Error(`tried to lookup ${elem} but result was undefined`);
      }
      console.log(`${elem} ${lookup}`);
      addrToArt.push({
        raw: address,
        address: address.address,
        metadata: lookup,
        filename: elem,
        product: product,
      });
    }
  }

  await writeFile(
    `./data/addressBatch/${batchIdentifier}-all.json`,
    JSON.stringify(addrToArt)
  );
  await writeFile(
    `./data/addressBatch/${batchIdentifier}-usedart.json`,
    JSON.stringify([
      ...indivFilesExcludes,
      ...usedArt,
      ...addrToArt.map((x) => x.filename),
    ])
  );

  const batches = [];
  function take(arr, len) {
    return arr.splice(0, len);
  }
  while (addrToArt.length > 0) {
    batches.push(take(addrToArt, 20));
  }
  for (let i = 0; i < batches.length; i++) {
    const batch = batches[i];
    await writeFile(
      `./data/addressBatch/${batchIdentifier}/${i}.json`,
      JSON.stringify(batch)
    );
  }
}

const nodeIndex = process.argv.findIndex((x) => x.includes("node"));
if (process.argv.length < nodeIndex + 3) {
  console.log(
    "missing parameter. example: node preprocess-addresses-for-mint.mjs <metadata-lookup-identifier> <batch-identifier> [last-batch]"
  );
  process.exit(1);
}

let usedArt = [];
if (process.argv.length > nodeIndex + 4) {
  usedArt = JSON.parse(
    readFileSync(
      `./data/addressBatch/${process.argv[nodeIndex + 4]}-usedart.json`
    )
  );
}

preprocess(process.argv[nodeIndex + 2], process.argv[nodeIndex + 3], usedArt)
  .then(() => process.exit(0))
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });
