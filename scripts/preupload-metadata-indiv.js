const { uploadMetadataToIPFS } = require("./util/upload");
const { readFile, writeFile } = require("fs/promises");

let lookup = {};

async function preuploadMetadata() {
  const metadata = JSON.parse(await readFile("./data/metadata-dryrun.json"));
  const imageIpfsLookup = JSON.parse(
    await readFile(`./data/cids/image-ipfs-lookup-indiv-4.json`)
  );
  const lookuptableEntries = Object.entries(imageIpfsLookup);
  console.log(metadata);
  const metadataArray = [];
  for (const artlookup of lookuptableEntries) {
    const data = metadata.find((x) => x.filename === artlookup[0]);
    if (!data) {
      throw new Error(`did not find ${artlookup[0]}`);
    }
    data.data.image = `ipfs://${artlookup[1]}`;
    metadataArray.push({ data: data.data, filename: artlookup[0] });
  }

  for (const data of metadataArray) {
    const ipfsMetaDataUploadResult = await uploadMetadataToIPFS(data.data);
    if (ipfsMetaDataUploadResult.IpfsHash) {
      lookup[data.filename] = ipfsMetaDataUploadResult.IpfsHash;
      console.log(
        `Uploaded metadata from ${data.filename} ${JSON.stringify(
          data
        )} to ipfs hash: ${ipfsMetaDataUploadResult.IpfsHash}`
      );
    } else {
      throw new Error(
        `there was an error uploading metadata for ${
          data.filename
        } ${JSON.stringify(data)} to ipfs.`
      );
    }
  }
}

preuploadMetadata()
  .then(() => {
    writeFile(
      `./data/cids/metadata-ipfs-lookup-indiv-3.json`,
      JSON.stringify(lookup)
    );
    console.log(
      `wrote cids to ./data/cids/metadata-ipfs-lookup-indiv-3.json ${
        Object.keys(lookup).length
      }`
    );
  })
  .catch((err) => {
    console.log(err);
    writeFile(
      `./data/cids/metadata-ipfs-lookup-indiv-3.json`,
      JSON.stringify(lookup)
    );
    console.log(`wrote cids to ./data/cids/metadata-ipfs-lookup-indiv-3.json`);
    process.exit(1);
  });
