const URI_BASE = "ipfs://";
const CONTRACT_CDI = "QmbE6HDAPZno1nwA1agjgWu2TJmcjNNp4hRuZWDLWqX9nM";
const CONTRACT_CDI_V2 = "QmWTqn9f4TJLMoU4Em5AhKQs4A5eoiqUv4VrxARo2MQE3h";
const CONTRACT_URI = URI_BASE + CONTRACT_CDI;
const CONTRACT_URI_V2 = URI_BASE + CONTRACT_CDI_V2;
const TOKEN_CDI = "QmWeAteYMPHXm2wHm4AfJ9m6cfcTufT93qn6P2axLvUZf5";
const ADMIN_ROLE =
  "0x0000000000000000000000000000000000000000000000000000000000000000";
const MINTER_ROLE =
  "0x9f2df0fed2c77648de5860a4cc508cd0818c85b8b8a1ab4ceeef8d981c8956a6";

const PROXY_REGISTRY = "0x58807baD0B376efc12F5AD86aAc70E78ed67deaE";

const ABI = [
  {
    constant: false,
    inputs: [
      {
        name: "_to",
        type: "address",
      },
      {
        name: "_tokenURI",
        type: "string",
      },
    ],
    name: "mintTo",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "totalSupply",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [],
    name: "owner",
    outputs: [
      {
        internalType: "address",
        name: "",
        type: "address",
      },
    ],
    stateMutability: "view",
    type: "function",
    constant: true,
  },
  {
    inputs: [],
    name: "proxyOwner",
    outputs: [
      {
        internalType: "address",
        name: "owner",
        type: "address",
      },
    ],
    stateMutability: "view",
    type: "function",
    constant: true,
  },
  {
    inputs: [],
    name: "isOwner",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [],
    name: "implementation",
    outputs: [
      {
        internalType: "address",
        name: "impl",
        type: "address",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [],
    name: "admin",
    outputs: [
      {
        internalType: "address",
        name: "admin_",
        type: "address",
      },
    ],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [],
    name: "caller",
    outputs: [
      {
        internalType: "address",
        name: "",
        type: "address",
      },
    ],
    stateMutability: "view",
    type: "function",
    constant: true,
  },
];

module.exports = {
  TOKEN_CDI,
  ABI,
  URI_BASE,
  CONTRACT_CDI,
  CONTRACT_CDI_V2,
  CONTRACT_URI,
  CONTRACT_URI_V2,
  ADMIN_ROLE,
  MINTER_ROLE,
  PROXY_REGISTRY
};
