const HDWalletProvider = require("@truffle/hdwallet-provider");
const { ABI, TOKEN_CDI } = require("./const");
const Web3 = require("web3");

const { MNEMONIC_OWNER, NETWORK, ALCHEMY_KEY } = process.env;

const WALLET_PROVIDER =
  "https://polygon-" + NETWORK + ".g.alchemy.com/v2/" + ALCHEMY_KEY;

function deployer(mnemonic, contract_address) {
  const wallet = new HDWalletProvider(mnemonic, WALLET_PROVIDER);
  const web3 = new Web3(wallet);
  const contract = new web3.eth.Contract(ABI, contract_address, {
    gasLimit: "10000000",
  });
  return contract;
}

function Eth(mnemonic) {
  const wallet = new HDWalletProvider(mnemonic, WALLET_PROVIDER);
  const web3 = new Web3(wallet);
  return web3.eth;
}

async function getReceipt(transactionHash) {
  const eth = Eth(MNEMONIC_OWNER);
  return eth.getTransactionReceipt(transactionHash);
}

async function mint(contract, _from) {
  return await contract.methods
    .mintTo(_from, TOKEN_CDI)
    .send({ from: _from, gas: 1000000 }, async (_error, transactionHash) => {
      let transactionReceipt = null;
      while (transactionReceipt == null) {
        // Waiting expectedBlockTime until the transaction is mined
        transactionReceipt = await getReceipt(transactionHash);
        await sleep(1000);
      }
    });
}

function sleep(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}


module.exports = {
  mint,
  deployer,
};

// async function fetchContractData(creature, proxy) {
//   const owner_creature = await creature.methods
//     .owner()
//     .call({ from: OWNER_ADDRESS });

//   const owner = await proxy.methods.owner().call({ from: OWNER_ADDRESS });

//   const implementation = await proxy.methods
//     .implementation()
//     .call({ from: ADMIN_ADDRESS });
//   const admin = await proxy.methods.admin().call({ from: ADMIN_ADDRESS });

//   console.log(
//     `Address of owner when calling Creature contract: ${owner_creature}\n`,
//     `Address of owner when calling Creature contract: ${owner}\n`,
//     `Address of the logic contract: ${implementation}\n`,
//     `Address of the admin of the proxy contract: ${admin}\n`
//   );
// }
