/* Contracts in this test */
const Creature = artifacts.require("./Creature.sol");
const truffleAssert = require("truffle-assertions");
const toBN = web3.utils.toBN;
const MockProxyRegistry = artifacts.require(
  "../contracts/MockProxyRegistry.sol"
);

contract("Creature", (accounts) => {
  const URI_BASE = "ipfs://";
  const CONTRACT_CDI = "QmbE6HDAPZno1nwA1agjgWu2TJmcjNNp4hRuZWDLWqX9nM";
  const CONTRACT_CDI_V2 = "QmWTqn9f4TJLMoU4Em5AhKQs4A5eoiqUv4VrxARo2MQE3h";
  const TOKEN_CDI = "QmWeAteYMPHXm2wHm4AfJ9m6cfcTufT93qn6P2axLvUZf5";
  const CONTRACT_URI = URI_BASE + CONTRACT_CDI;
  const CONTRACT_URI_V2 = URI_BASE + CONTRACT_CDI_V2;
  let creatures;
  let proxy;

  const owner = accounts[0];
  const userA = accounts[1];
  const proxyForOwner = accounts[8];

  // To install the proxy mock and the attack contract we deploy our own
  // instances of all the classes here rather than using the ones that Truffle
  // deployed.

  before(async () => {
    // creatureProxy = await Proxy.deployed();
    // await creatureProxy.upgradeTo(creatures.address);
    proxy = await MockProxyRegistry.new();
    await proxy.setProxy(owner, proxyForOwner);

    creatures = await Creature.new({ from: owner });
    await creatures.init(proxy.address, CONTRACT_CDI, { from: owner });
  });

  // This is all we test for now

  // This also tests contractURI()

  describe("#constructor()", () => {
    it("should set the contractURI to the supplied value", async () => {
      assert.equal(await creatures.contractURI(), CONTRACT_URI);
    });
  });

  describe("#name()", () => {
    it("should return the correct name", async () => {
      assert.equal(await creatures.name(), "Creature");
    });
  });

  describe("#symbol()", () => {
    it("should return the correct symbol", async () => {
      assert.equal(await creatures.symbol(), "CSF");
    });
  });

  describe("#setContractURI()", () => {
    it("should set the contractURI to the new contractURI", async () => {
      await creatures.setContractURI(CONTRACT_CDI_V2);
      assert.equal(await creatures.contractURI(), CONTRACT_URI_V2);
    });
  });

  describe("#mint()", () => {
    it("should not allow non-owner or non-operator to mint", async () => {
      await truffleAssert.fails(
        creatures.mintTo(userA, TOKEN_CDI, { from: userA }),
        truffleAssert.ErrorType.REVERT
      );
    });

    it("should allow owner to mint", async () => {
      await creatures.mintTo(userA, TOKEN_CDI, {
        from: owner,
      });
      // Check that the recipient got token
      const balanceUserA = await creatures.balanceOf(userA);
      assert.isOk(balanceUserA.eq(toBN(1)));
    });

    it("should not allow proxy to mint", async () => {
      await truffleAssert.fails(
        creatures.mintTo(proxyForOwner, TOKEN_CDI, { from: proxyForOwner }),
        truffleAssert.ErrorType.REVERT
      );
    });
  });
  describe("#burn()", () => {
    it("should allow the owner to burn the token", async () => {
      await creatures.burn(1, {
        from: userA,
      });
      const balanceUserA = await creatures.balanceOf(userA);
      assert.isOk(balanceUserA.eq(toBN(0)));
    });
    it("should not allow the non-owner to burn the token", async () => {
      await creatures.mintTo(userA, TOKEN_CDI, {
        from: owner,
      });
      await truffleAssert.fails(
        creatures.burn(2, { from: owner }),
        truffleAssert.ErrorType.REVERT
      );
    });
  });
});
