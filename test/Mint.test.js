const HDWalletProvider = require("@truffle/hdwallet-provider");
const Web3 = require("web3");
const address = require("../contracts.json");
const { ABI } = require("./util/const");
const {
  OWNER_ADDRESS,
  ADMIN_ADDRESS,
  MNEMONIC_OWNER,
  MNEMONIC_ADMIN,
  NETWORK,
  PROVIDER,
  INFURA_KEY,
  ALCHEMY_KEY,
} = process.env;
const NODE_API_KEY = INFURA_KEY || ALCHEMY_KEY;
const { proxy: PROXY_ADDRESS, nft: CREATURE_ADDRESS } = address;
const TOKEN_CDI = "QmWeAteYMPHXm2wHm4AfJ9m6cfcTufT93qn6P2axLvUZf5";
if (
  !MNEMONIC_ADMIN ||
  !MNEMONIC_OWNER ||
  !NODE_API_KEY ||
  !OWNER_ADDRESS ||
  !NETWORK
) {
  console.error(
    "Please set a mnemonic, Alchemy/Infura key, owner, network, and contract address."
  );
  return;
}

let network;
switch (NETWORK) {
  case "mainnet":
  case "live":
    network = "mainnet";
    break;
  case "mumbai":
    network = NETWORK;
}
let WALLET_PROVIDER;
switch (PROVIDER) {
  case "infura":
    WALLET_PROVIDER = "https://" + network + ".infura.io/v3/" + NODE_API_KEY;
    break;
  case "alchemy":
    WALLET_PROVIDER =
      "https://eth-" + network + ".alchemyapi.io/v2/" + NODE_API_KEY;
    break;
  case "alchemy-mumbai":
    WALLET_PROVIDER =
      "https://polygon-" + network + ".g.alchemy.com/v2/" + NODE_API_KEY;
    break;
}

function deployer(mnemonic, contract_address) {
  const wallet = new HDWalletProvider(mnemonic, WALLET_PROVIDER);
  const web3 = new Web3(wallet);
  const contract = new web3.eth.Contract(ABI, contract_address, {
    gasLimit: "10000000",
  });
  return contract;
}

async function fetchContractData(creature, proxy) {
  const owner_creature = await creature.methods
    .owner()
    .call({ from: OWNER_ADDRESS });

  const owner = await proxy.methods.owner().call({ from: OWNER_ADDRESS });

  const implementation = await proxy.methods
    .implementation()
    .call({ from: ADMIN_ADDRESS });
  const admin = await proxy.methods.admin().call({ from: ADMIN_ADDRESS });

  console.log(
    `Address of owner when calling Creature contract: ${owner_creature}`,
    `\nAddress of owner when calling Creature contract: ${owner}`,
    `\nAddress of the logic contract: ${implementation}`,
    `\nAddress of the admin of the proxy contract: ${admin}`
  );
}

function Eth(mnemonic) {
  const wallet = new HDWalletProvider(mnemonic, WALLET_PROVIDER);
  const web3 = new Web3(wallet);
  return web3.eth;
}

async function getReceipt(transactionHash) {
  const eth = Eth(MNEMONIC_OWNER);
  return eth.getTransactionReceipt(transactionHash);
}

function sleep(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}

async function mint(contract, _from) {
  try {
    const { blockHash, to, transactionHash, from } = await contract.methods
      .mintTo(_from, TOKEN_CDI)
      .send({ from: _from, gas: 1000000 }, async (_error, transactionHash) => {
        let transactionReceipt = null;
        while (transactionReceipt == null) {
          // Waiting expectedBlockTime until the transaction is mined
          transactionReceipt = await getReceipt(transactionHash);
          await sleep(1000);
        }
      });
    console.log(
      `Transaction successful!`,

      { from, to, blockHash, transactionHash }
    );
  } catch (e) {
    let receipt = e.receipt;
    if (receipt) {
      const { blockHash, to, transactionHash, from } = receipt;
      console.log(`An error occured while sending a transaction!`, {
        from,
        to,
        blockHash,
        transactionHash,
      });
    }
  }
}

async function main() {
  const ownerToCreature = deployer(MNEMONIC_OWNER, CREATURE_ADDRESS);
  const adminToCreature = deployer(MNEMONIC_ADMIN, CREATURE_ADDRESS);
  const ownerToProxy = deployer(MNEMONIC_OWNER, PROXY_ADDRESS);
  const adminToProxy = deployer(MNEMONIC_ADMIN, PROXY_ADDRESS);
  await fetchContractData(ownerToCreature, ownerToProxy);
  await mint(ownerToCreature, OWNER_ADDRESS);
  await mint(adminToCreature, ADMIN_ADDRESS);
  await mint(ownerToProxy, OWNER_ADDRESS);
  await mint(adminToProxy, ADMIN_ADDRESS);
  process.exit(0);
}

main();
