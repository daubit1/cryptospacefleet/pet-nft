/* Contracts in this test */
const CSF = artifacts.require("./Cryptospacefleet.sol");
const truffleAssert = require("truffle-assertions");
const MockCreature = artifacts.require("../contracts/MockCryptospacefleet.sol");
const MockProxyRegistry = artifacts.require(
  "../contracts/MockProxyRegistry.sol"
);
const { deployProxy, upgradeProxy } = require("@openzeppelin/truffle-upgrades");
const toBN = web3.utils.toBN;
const {
  CONTRACT_CDI,
  ADMIN_ROLE,
  MINTER_ROLE,
  CONTRACT_CDI_V2,
  CONTRACT_URI,
  CONTRACT_URI_V2,
  TOKEN_CDI,
  PROXY_REGISTRY,
} = require("./util/const");

contract("Cryptospacefleet Contracts", (accounts) => {
  let csf;
  let mockCsf;
  let mockProxyRegistry;
  let NEW_PROXY_REGISTRY_ADDRESS;
  const owner = accounts[0]; //Owner of the csf contract
  const admin = accounts[1]; //Admin of the proxy contract,
  const userA = accounts[2];
  const proxyForOwner = accounts[8];
  describe("Interacting with V1 contract", () => {
    before(async () => {
      mockProxyRegistry = await MockProxyRegistry.new();
      await mockProxyRegistry.setProxy(owner, proxyForOwner);
      NEW_PROXY_REGISTRY_ADDRESS = mockProxyRegistry.address;
      csf = await deployProxy(CSF, [PROXY_REGISTRY, CONTRACT_CDI]);
    });
    describe("Proxy", () => {
      describe("#grantRole()", () => {
        it("can grant admin ownership", async () => {
          truffleAssert.passes(await csf.grantRole(ADMIN_ROLE, admin));
        });
      });
      describe("#grantRole()", () => {
        it("should error when send from non-admin", async () => {
          try {
            await csf.grantRole(ADMIN_ROLE, userA, { from: userA });
          } catch (e) {
            const { reason } = e;
            assert.isOk(
              reason.includes("AccessControl") &&
                reason.includes("missing role")
            );
          }
        });
      });
    });
    describe("Roles", () => {
      it("Owner is admin", async () => {
        assert.isOk(await csf.hasRole(ADMIN_ROLE, owner));
      });
      it("Owner is minter", async () => {
        assert.isOk(await csf.hasRole(MINTER_ROLE, owner));
      });
      it("Admin is admin", async () => {
        assert.isOk(await csf.hasRole(ADMIN_ROLE, admin));
      });
      it("Admin is NOT minter", async () => {
        assert.isFalse(await csf.hasRole(MINTER_ROLE, admin));
      });
      it("User is NOT admin", async () => {
        assert.isFalse(await csf.hasRole(ADMIN_ROLE, userA));
      });
      it("User is NOT minter", async () => {
        assert.isFalse(await csf.hasRole(MINTER_ROLE, userA));
      });
    });

    describe("Cryptospacefleet", () => {
      describe("#constructor()", () => {
        it("should set the contractURI to the supplied value", async () => {
          assert.equal(await csf.contractURI(), CONTRACT_URI);
        });
      });
      describe("#name()", () => {
        it("should return the correct name", async () => {
          assert.equal(await csf.name(), "Cryptospacefleet");
        });
      });
      describe("#baseTokenURI()", () => {
        it("should return ipfs://", async () => {
          assert.equal(await csf.baseTokenURI(), "ipfs://");
        });
      });
      describe("#symbol()", () => {
        it("should return the correct symbol", async () => {
          assert.equal(await csf.symbol(), "CSF");
        });
      });
      describe("#setContractURI()", () => {
        it("should set the contractURI only from admin", async () => {
          await truffleAssert.passes(await csf.setContractURI(CONTRACT_CDI_V2));
          assert.equal(await csf.contractURI(), CONTRACT_URI_V2);
        });
        it("should not set the contractURI from non-admins", async () => {
          await truffleAssert.fails(
            csf.setContractURI(CONTRACT_CDI_V2, { from: userA }),
            truffleAssert.ErrorType.REVERT
          );
        });
      });
      describe("#setProxyAddress()", () => {
        it("should revert when setting same proxy address", async () => {
          await truffleAssert.fails(
            csf.setProxyAddress(PROXY_REGISTRY, { from: owner }),
            truffleAssert.ErrorType.REVERT
          );
        });
        it("should revert when set from non-owner", async () => {
          await truffleAssert.fails(
            csf.setProxyAddress(PROXY_REGISTRY, { from: userA }),
            truffleAssert.ErrorType.REVERT
          );
        });
        it("should set new proxy address when called from owner", async () => {
          await truffleAssert.passes(
            csf.setProxyAddress(NEW_PROXY_REGISTRY_ADDRESS, { from: owner })
          );
        });
      });
      describe("#isApprovedForAll()", () => {
        it("should return false when approving for owner", async () => {
          assert.isFalse(
            await csf.isApprovedForAll(userA, owner, {
              from: owner,
            })
          );
        });
        it("should approve when called with proxy registry", async () => {
          assert.isOk(
            await csf.isApprovedForAll(userA, NEW_PROXY_REGISTRY_ADDRESS, {
              from: owner,
            })
          );
        });
      });
    });
    describe("Minting", () => {
      describe("#mint()", () => {
        it("should not allow non-owner or non-operator to mint", async () => {
          await truffleAssert.fails(
            csf.mintTo(userA, TOKEN_CDI, { from: userA }),
            truffleAssert.ErrorType.REVERT
          );
        });
        it("should allow owner to mint", async () => {
          await truffleAssert.passes(
            await csf.mintTo(userA, TOKEN_CDI, {
              from: owner,
            })
          );
        });
        it("should not allow owner to mint with empty CDI", async () => {
          await truffleAssert.fails(
            csf.mintTo(userA, "", { from: owner }),
            truffleAssert.ErrorType.REVERT,
            "Error: Token URI is empty!"
          );
        });
      });
      describe("#burn()", () => {
        it("should not allow the non-owner to burn the token", async () => {
          await truffleAssert.fails(
            csf.burn(1, { from: owner }),
            truffleAssert.ErrorType.REVERT
          );
        });
        it("should allow the owner to burn the token", async () => {
          truffleAssert.passes(
            await csf.burn(1, {
              from: userA,
            })
          );
          const balanceUserA = await csf.balanceOf(userA);
          assert.isOk(balanceUserA.eq(toBN(0)));
        });
      });
      describe("#totalSupply()", () => {
        it("should return the correct amount", async () => {
          assert.equal(await csf.totalSupply(), 1);
        });
      });
    });

    describe("Upgrading to V2", () => {
      before(async () => {
        mockCsf = await upgradeProxy(csf, MockCreature);
      });
      describe("#mint()", () => {
        it("should not allow non-owner or non-operator to mint", async () => {
          await truffleAssert.fails(
            mockCsf.mintTo(userA, TOKEN_CDI, { from: userA }),
            truffleAssert.ErrorType.REVERT
          );
        });

        it("should allow owner to mint", async () => {
          await truffleAssert.passes(
            await mockCsf.mintTo(userA, TOKEN_CDI, {
              from: owner,
            })
          );
        });
        it("should not allow owner to mint with empty CDI", async () => {
          await truffleAssert.fails(
            mockCsf.mintTo(userA, "", { from: owner }),
            truffleAssert.ErrorType.REVERT,
            "Error: Token URI is empty!"
          );
        });
      });
      describe("#burn()", () => {
        it("should allow owner to burn the token", async () => {
          await mockCsf.burn(2, {
            from: userA,
          });
          const balanceUserA = await mockCsf.balanceOf(userA);
          assert.isOk(balanceUserA.eq(toBN(0)));
        });
        it("should not allow the non-owner to burn the token", async () => {
          await mockCsf.mintTo(userA, TOKEN_CDI, {
            from: owner,
          });
          await truffleAssert.fails(
            mockCsf.burn(3, { from: owner }),
            truffleAssert.ErrorType.REVERT
          );
        });
      });
    });
    describe("Total supply", () => {
      it("should return the new amount of minted tokens", async () => {
        let totalSupply = await mockCsf.totalSupply();
        assert.isOk(totalSupply.eq(toBN(3)));
      });
    });
  });
});
