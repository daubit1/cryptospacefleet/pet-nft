const CSF = artifacts.require("./Cryptospacefleet.sol");
const { deployProxy, upgradeProxy } = require("@openzeppelin/truffle-upgrades");
const fs = require("fs");
const Web3 = require("web3");
const testnet_path = "./contracts/adresses/testnet.json";
const mainnet_path = "./contracts/adresses/mainnet.json";
const develop_path = "./contracts/adresses/development.json";
const IMPLEMENTATION_POSITION =
  "0x360894a13ba1a3210667c828492db98dca3e2076cc3735a920a3ca505d382bbc";
const CONTRACT_CID = "QmfDBBDeW3K8UbMVPssg769NF6qpiByuswTuGtNTzC19hg";
const REGISTRY_ADDRESS = "0x58807baD0B376efc12F5AD86aAc70E78ed67deaE";
module.exports = async (deployer, network) => {
  const address_path = {
    development: develop_path,
    mumbai: testnet_path,
    polygon: mainnet_path,
  }[network];
  const ozData = fs.readFileSync(address_path, "utf8", (err, data) => {
    return err ? {} : data;
  });

  let csf;
  let proxy;
  try {
    csf = await CSF.deployed();
  } catch (e) {
    csf = null;
  }
  if (!ozData.proxy || !csf) {
    proxy = await deployProxy(CSF, [REGISTRY_ADDRESS, CONTRACT_CID], {
      deployer,
    });
  } else {
    proxy = await upgradeProxy(csf.address, CSF);
  }
  const proxy_address = proxy.address;
  const web3 = new Web3(deployer.provider);
  const result = await web3.eth.getStorageAt(
    proxy_address,
    IMPLEMENTATION_POSITION
  );
  const csf_address =
    result.length === 66 ? `0x${result.slice(26, 66)}` : result;
  // write file to disk
  if (address_path) {
    const addresses = JSON.stringify({
      proxy_address,
      csf_address,
    });
    fs.writeFile(address_path, addresses, "utf8", (err) => {
      if (err) {
        console.log(`Error writing file: ${err}`);
      }
    });
  }
};
