const HDWalletProvider = require("@truffle/hdwallet-provider");

const MNEMONIC = process.env.MNEMONIC;
const TESTNET_API_KEY = process.env.ALCHEMY_KEY_TESTNET;
const MAINNET_API_KEY = process.env.ALCHEMY_KEY_MAINNET;
const POLYSCAN_KEY = process.env.POLYSCAN_KEY;

if (!MNEMONIC && (!ALCHEMY_KEY_MAINNET || !ALCHEMY_KEY_TESTNET)) {
  console.error("Please set a mnemonic and ALCHEMY_KEY");
  process.exit(0);
}

// "https://rpc-mumbai.maticvigil.com/"
// "wss://ws-matic-mumbai.chainstacklabs.com"
const mumbaiNodeUrl =
  "wss://polygon-mumbai.g.alchemy.com/v2/" + TESTNET_API_KEY;
const polygonNodeUrl =
  "wss://polygon-mainnet.g.alchemy.com/v2/" + MAINNET_API_KEY;

module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 7545,
      network_id: "*", // Match any network id
    },
    mumbai: {
      network_id: 80001,
      chainId: 80001,
      provider: function () {
        return new HDWalletProvider({
          mnemonic: {
            phrase: MNEMONIC,
          },
          providerOrUrl: mumbaiNodeUrl,
          pollingInterval: 10000,
        });
      },
      confirmations: 2,
      networkCheckTimeout: 2000000,
      timeoutBlocks: 200,
      skipDryRun: true,
    },
    polygon: {
      network_id: 137,
      chainId: 137,
      provider: function () {
        return new HDWalletProvider({
          mnemonic: {
            phrase: MNEMONIC,
          },
          providerOrUrl: polygonNodeUrl,
          pollingInterval: 10000,
        });
      },
      confirmations: 2,
      networkCheckTimeout: 2000000,
      timeoutBlocks: 200,
      skipDryRun: true,
    },
  },
  mocha: {
    reporter: "eth-gas-reporter",
    reporterOptions: {
      currency: "USD",
      token: "MATIC",
      gasPriceApi:
        "https://api.polygonscan.com/api?module=proxy&action=eth_gasPrice",
    },
  },
  compilers: {
    solc: {
      version: "^0.8.11",
      settings: {
        optimizer: {
          enabled: true,
          runs: 200, // Optimize for how many times you intend to run the code
        },
      },
    },
  },
  plugins: [
    "truffle-plugin-verify",
    "solidity-coverage",
    "@chainsafe/truffle-plugin-abigen",
  ],
  api_keys: {
    polygonscan: POLYSCAN_KEY,
  },
};
