// SPDX-License-Identifier: MIT
pragma solidity 0.8.11;

import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721BurnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721URIStorageUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlEnumerableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/utils/CountersUpgradeable.sol";
import "./common/meta-transactions/ContentMixin.sol";
import "./common/meta-transactions/NativeMetaTransaction.sol";

/**
 * @title ERC721Tradeable
 * ERC721Tradeable - ERC721 contract that whitelists a trading address, and has minting functionality.
 */
abstract contract ERC721TradeableUpgradeable is
    Initializable,
    ERC721Upgradeable,
    ERC721URIStorageUpgradeable,
    ERC721BurnableUpgradeable,
    ContextMixin,
    AccessControlEnumerableUpgradeable,
    NativeMetaTransaction
{
    using CountersUpgradeable for CountersUpgradeable.Counter;

    /**
     * We rely on the OZ Counter util to keep track of the next available ID.
     * We track the nextTokenId instead of the currentTokenId to save users on gas costs.
     * Read more about it here: https://shiny.mirror.xyz/OUampBbIz9ebEicfGnQf5At_ReMHlZy0tB4glb9xQ0E
     */
    CountersUpgradeable.Counter private _nextTokenId;
    address proxyRegistryAddress;

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

    function __ERC721Tradeable_init(
        string memory _name,
        string memory _symbol,
        address _proxyRegistryAddress
    ) internal onlyInitializing {
        __ERC721_init(_name, _symbol);
        __ERC721URIStorage_init();
        __ERC721Burnable_init();
        _initializeEIP712(_name);
        // nextTokenId is initialized to 1, since starting at 0 leads to higher gas cost for the first minter
        _nextTokenId.increment();
        proxyRegistryAddress = _proxyRegistryAddress;

        __AccessControl_init();
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(MINTER_ROLE, msg.sender);
    }

    /**
     * @dev Mints a token to an address with a tokenURI.
     * @param _to address of the future owner of the token
     * @param _tokenURI uri of the token metadata
     */
    function mintTo(address _to, string memory _tokenURI)
        external
        onlyRole(MINTER_ROLE)
    {
        require(bytes(_tokenURI).length != 0, "Error: Token URI is empty!");
        uint256 currentTokenId = _nextTokenId.current();
        _nextTokenId.increment();
        _safeMint(_to, currentTokenId);
        _setTokenURI(currentTokenId, _tokenURI);
    }

    /**
        @dev Returns the total tokens minted so far.
        1 is always subtracted from the Counter since it tracks the next available tokenId.
     */
    function totalSupply() external view returns (uint256) {
        return _nextTokenId.current() - 1;
    }

    /**
     * @dev Returns the base URI for every URI.
     */
    function baseTokenURI() public view virtual returns (string memory);

    function _baseURI() internal view override returns (string memory) {
        return baseTokenURI();
    }

    /**
     * Override isApprovedForAll to whitelist user's OpenSea proxy accounts to enable gas-less listings.
     */
    function isApprovedForAll(address owner, address operator)
        public
        view
        override
        returns (bool)
    {
        // Whitelist OpenSea proxy contract for easy trading.
        if (address(proxyRegistryAddress) == operator) {
            return true;
        }
        return super.isApprovedForAll(owner, operator);
    }

    /**
     * @dev Allows the contract owner to upgrade the address of the proxy registry.
     * @param _proxyRegistryAddress representing the address of the proxy registry
     */
    function setProxyAddress(address _proxyRegistryAddress)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        require(
            _proxyRegistryAddress != proxyRegistryAddress &&
                _proxyRegistryAddress != address(0),
            "Invalid address!"
        );
        proxyRegistryAddress = _proxyRegistryAddress;
    }

    /**
     * This is used instead of msg.sender as transactions won't be sent by the original token owner, but by OpenSea.
     */
    function _msgSender() internal view override returns (address sender) {
        return ContextMixin.msgSender();
    }

    function _burn(uint256 tokenId)
        internal
        override(ERC721Upgradeable, ERC721URIStorageUpgradeable)
    {
        super._burn(tokenId);
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721Upgradeable, ERC721URIStorageUpgradeable)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721Upgradeable, AccessControlEnumerableUpgradeable)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
}
