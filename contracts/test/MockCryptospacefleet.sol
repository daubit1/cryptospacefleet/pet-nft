// SPDX-License-Identifier: MIT

pragma solidity ^0.8.11;

import "../ERC721TradeableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

/**
 * @title Cryptospacefleet
 * Cryptospacefleet - a contract for my non-fungible creatures.
 */
contract MockCryptospacefleet is Initializable, ERC721TradeableUpgradeable {
    string private _contractURI;

    /**
     * @dev the initializer sets the proxyRegistryAddress and the contract URI of the contract.
     * @param _proxyRegistryAddress representing the address of the OpenSea Proxy Registry
     * @param contractURI_ representing the CDI of the contract.
     */
    function init(address _proxyRegistryAddress, string memory contractURI_)
        public
        initializer
    {
        __ERC721Tradeable_init(
            "MockCryptospacefleet",
            "MCSF",
            _proxyRegistryAddress
        );
        _contractURI = contractURI_;
    }

    /**
     * @dev Returns the base URI for every URI.
     */
    function baseTokenURI() public pure override returns (string memory) {
        return "https://";
    }

    /**
     * @dev Allows the proxy owner to upgrade the current version of the proxy.
     * @param contractURI_ representing the address of the new implementation to be set.
     */
    function setContractURI(string memory contractURI_)
        public
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        _contractURI = contractURI_;
    }

    /**
     * @dev Returns the contract URI.
     */
    function contractURI() public view returns (string memory) {
        return string(abi.encodePacked(baseTokenURI(), _contractURI));
    }
}
